
var appInfo01: Set = ["A" , "B" , "C"]
appInfo01.insert("C")
print("--- 1 ---")
for val in appInfo01 {
    print("\(val)")
}

var appInfo02: Set = ["A" , "B" , "D"]
var showIntersection = appInfo01.intersection(appInfo02)
print("--- Intersection ---")
for val in showIntersection {
    print("\(val)")
}

print("--- SymmetricDifference--- ")

var showSymmetricDifference = appInfo01.symmetricDifference(appInfo02)

for val in showSymmetricDifference {
    print("\(val)")
}

