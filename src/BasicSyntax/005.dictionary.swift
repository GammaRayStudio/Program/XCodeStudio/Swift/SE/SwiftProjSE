print("Dictionaries\n------")

var dict = ["A" : "001" , "B" : "002" , "C" : "003"]

for(key,val) in dict{
    print("\(key) : \(val)")
}

print("---")
for code in dict.keys {
    print("\(code)")
}

print("---")
for val in dict.values{
    print("\(val)")
}

print("\nDictionaries Declare\n------")

var dicts: [ String : String ] =  ["A" : "001" , "B" : "002" , "C" : "003"] 

for code in dicts.keys {
    print("\(code)")
}

print("---")
for val in dicts.values{
    print("\(val)")
}