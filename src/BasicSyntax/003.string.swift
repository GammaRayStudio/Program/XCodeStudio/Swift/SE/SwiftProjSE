// 文字處理
var message = "hello SwiftProjSE"
var msgLen = message.count
var part01 = "Hi ~"
var part02 = "!!!"
var allMsg = part01 + part02

var uppperText = "abcdefg"
uppperText = uppperText.uppercased()

var lowerText = "ABCDEFG"
lowerText = lowerText.lowercased()

var total = 100
var showTextInDiffType01 = "01: total = " + String(total)
var showTextInDiffType02 = "02: total = \(total)"

var actual = "Msg"
var expect = "Msg"
var isEquals = false
if expect == actual {
    isEquals = true
}

print(message)
print(msgLen)
print(allMsg)
print(uppperText)
print(lowerText)
print(showTextInDiffType01)
print(showTextInDiffType02)
print("isEquals = \(isEquals)")

