print(" --- Arrays --- ")

var arrays = ["001" , "002" , "003"]
for msg in arrays{
    print(msg)
}

print(" --- Array Declare --- ")

var messages : [String] = ["001" , "002" , "003"]
messages += ["004" , "005"]
for msg in messages {
    print(msg)
}

print(" --- Array Append --- ")

messages[1...3] = ["A" , "B" , "C"]
for msg in messages {
    print(msg)
}