var flag : Int = 1
switch flag{
    case 1: 
        print("switch => flag = 1")
    default:
        print("switch => default")
}

var scope = 125
switch scope {
    case 0...50:
        print("scope => 0...")
    case 50...101:
        print("scopt => 50...101")
    case 101..<150:
        print("scope => 101..<150")
    default:
        print("switch => default")
}