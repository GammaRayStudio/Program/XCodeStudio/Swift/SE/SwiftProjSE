// for - 01
print("--- 1 ---")

for index in 0...2{
    print(index)    
}

print("--- 2 ---")

// for - 02
for index in 0..<3{
    print(index)    
}

print("--- 3 ---")

var arrays = ["1" , "2" , "3"]
// for - 03
for index in 0...arrays.count-1{
    print(index)    
}